const input1 = document.querySelectorAll(".input-1");
const input2 = document.querySelectorAll(".input-2");
const input3 = document.querySelectorAll(".input-3");
const input4 = document.querySelectorAll(".input-4");

function focusFunc() {
	let parent = this.parentNode
	parent.classList.add("focus");
}

function blurFunc() {
	let parent = this.parentNode
	if(this.value == ""){
		parent.classList.remove("focus");
	}
}

input1.forEach(input1  => {
	input1.addEventListener("focus", focusFunc);
	input1.addEventListener("blur", blurFunc);
});

input2.forEach(input2  => {
	input2.addEventListener("focus", focusFunc);
	input2.addEventListener("blur", blurFunc);
});

input3.forEach(input3  => {
	input3.addEventListener("focus", focusFunc);
	input3.addEventListener("blur", blurFunc);
});

input4.forEach(input4  => {
	input4.addEventListener("focus", focusFunc);
	input4.addEventListener("blur", blurFunc);
});