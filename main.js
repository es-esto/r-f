$(document).ready(function () {
	$('.submit').click(function (event) {
		console.log('Clicked button')	

		var ussername = $('.input-1').val()
		var email = $('.input-2').val()
		var phone = $('.input-3').val()
		var message = $('.input-4').val()
		var statusElm = $('.status')
		statusElm.empty()


		if(ussername.length >= 2) {
			statusElm.append('<div>Ussername is valid</div>')
		} else {
			event.preventDefault()
			statusElm.append('<div>Ussername is not valid</div>')
		} 

		if(email.length > 5 && email.includes('@') && email.includes('.')) {
			statusElm.append('<div>Email is valid</div>')
		} else {
			event.preventDefault()
			statusElm.append('<div>Email is not valid</div>')
		}

		if(message.length > 10) {
			statusElm.append('<div>Message is valid</div>')
		} else {		
			event.preventDefault()
			statusElm.append('<div>Message is not valid</div>')
		} 

	})
})